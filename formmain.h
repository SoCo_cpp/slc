//-------------------------------------------------------
#ifndef FORMMAIN_H
#define FORMMAIN_H
//-------------------------------------------------------
#include <QMainWindow>
//-------------------------------------------------------
namespace Ui {
    class formMain;
}
//-------------------------------------------------------
class formMain : public QMainWindow
{
    Q_OBJECT
public:
    //----------------------
    //----------------------
//----------------------
    formMain(QWidget *parent = 0);
    ~formMain();
    //----------------------
protected:
    //----------------------
    void changeEvent(QEvent *e);
    //----------------------
private:
    //----------------------
    Ui::formMain *ui;
    QStringList fileList;
    void iterateDirectory(const QString& strDirectory, bool isRecursive, const QStringList& slFilters);
    int countFileLines(const QString& strFileName);
    void addResult(int lineCount, const QString& strFileName);
    //----------------------
private slots:
    //----------------------
    void on_lblWebLink_linkActivated(QString link);
    void on_btnRemoveComment_clicked();
    void on_btnAddComment_clicked();
    void on_btnRemoveFilter_clicked();
    void on_btnAddFilter_clicked();
    void on_btnBrowse_clicked();
    void on_btnGo_clicked();
    void save();
    void load();
    //----------------------
}; // class formMain : public QMainWindow
//-------------------------------------------------------
#endif // FORMMAIN_H
//-------------------------------------------------------
