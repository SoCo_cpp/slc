#ifndef NUMTABLEWIDGETITEM_H
#define NUMTABLEWIDGETITEM_H

#include <QTableWidgetItem>
const int NumTableWidgetItemType = QTableWidgetItem::UserType + 1;


class NumTableWidgetItem : public QTableWidgetItem
{
    //Q_OBJECT
public:
    explicit NumTableWidgetItem();
    explicit NumTableWidgetItem(int value);
    virtual bool operator<(const QTableWidgetItem & other) const;
    int value() const;
    void setValue(int value);
private:
    int m_value;
signals:

public slots:

};

#endif // NUMTABLEWIDGETITEM_H
