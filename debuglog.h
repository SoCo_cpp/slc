#ifndef DEBUGLOG_H
#define DEBUGLOG_H
//---------------------------------------------------------------------------
#define DebugLog_FormatBufferSize    4096
#define DebugLog_FileNameBufferSize  1024
#define DebugLog_FileName   "DebugLog.txt"
//---------------------------------------------------------------------------
extern char strDebugLogFileName[DebugLog_FileNameBufferSize];
//---------------------------------------------------------------------------
void DebugLog(const char* str, ...);
//---------------------------------------------------------------------------
#endif // DEBUGLOG_H





