#include "numtablewidgetitem.h"

NumTableWidgetItem::NumTableWidgetItem() :  QTableWidgetItem(NumTableWidgetItemType)
{

}
NumTableWidgetItem::NumTableWidgetItem(int value)  :  QTableWidgetItem(NumTableWidgetItemType)
{
    setValue(value);
}
bool NumTableWidgetItem::operator<(const QTableWidgetItem & other) const
{
    if (other.type() != NumTableWidgetItemType)
        return false;
    NumTableWidgetItem* numItem = (NumTableWidgetItem*)&other;
    return (m_value < numItem->value());
}
int NumTableWidgetItem::value() const
{
    return m_value;
}
void NumTableWidgetItem::setValue(int value)
{
    m_value = value;
    setText(QString().number(m_value));
}
