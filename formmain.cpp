#include "formmain.h"
#include "ui_formmain.h"
#include <QFileDialog>
#include <QTextStream>
#include <QLineEdit> // used with a combo box
#include "debuglog.h"
#include <QInputDialog>
#include <QSettings>
#include <QDesktopServices>
#include <QUrl>
#include "numtablewidgetitem.h"

//-------------------------------------------------------
formMain::formMain(QWidget *parent) : QMainWindow(parent),
    ui(new Ui::formMain)
{

    ui->setupUi(this);
    ui->tableResult->setRowCount(1);
    ui->tableResult->setColumnCount(2);

    QTableWidgetItem* columnCount = new QTableWidgetItem(tr("Count"));
    QTableWidgetItem* columnFilename = new QTableWidgetItem(tr("Filename"));
    //columnCount->setTextAlignment(Qt::AlignRight);
    ui->tableResult->setHorizontalHeaderItem(0,columnCount);
    ui->tableResult->setHorizontalHeaderItem(1, columnFilename);
    load();
}
//-------------------------------------------------------
formMain::~formMain()
{
    delete ui;
}
//-------------------------------------------------------
void formMain::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
//-------------------------------------------------------
void formMain::on_btnGo_clicked()
{
    int i;
    QStringList nameFilters;
    QDir dir(ui->cbDirectory->currentText());
    if (!dir.exists())
    {
        ui->textResult->setText(tr("Root source directory does not exist: ") + ui->cbDirectory->currentText());
        return;
    }
    for (i = 0;i < ui->lstFilters->count();++i)
        nameFilters.append(ui->lstFilters->item(i)->text());

    if (!ui->cbDirectory->currentText().isEmpty() && ui->cbDirectory->findText(ui->cbDirectory->currentText()) == -1)
        ui->cbDirectory->insertItem(0, ui->cbDirectory->currentText());

    ui->tableResult->setRowCount(0);
    ui->tableResult->sortItems(1); // make sure not sorted by count, this prevents filename column from being filled properly
    fileList.clear();
    iterateDirectory(ui->cbDirectory->currentText(), ui->cbRecursive->isChecked(), nameFilters);

    int cnt = 0;
    for (i = 0;i < fileList.count();++i)
      cnt += countFileLines(fileList.at(i));
    ui->textResult->setText(tr("Total files: <strong>") + QString().setNum(fileList.count()) + tr("</strong> Total lines: <strong>") + QString().setNum(cnt) + "</strong>");

    save();
}
//-------------------------------------------------------
void formMain::iterateDirectory(const QString& strDirectory, bool isRecursive, const QStringList& slFilters)
{
    DebugLog("iterateDirectory - r %c '%s'", (isRecursive ? 'Y' : 'N'), qPrintable(strDirectory));
    QDir dir(strDirectory);
    if (!dir.exists())
    {
        // ignore non existant directories ( /.. /.
    }
    else if (!dir.isReadable())
    {
        ui->textResult->setText(tr("<span style=\"color: red;\">") + ui->cbDirectory->currentText() + tr(" is not readable.</span>"));
    }
    else
    {
        QFileInfoList fileInfoList;
        QDir::Filters filters = QDir::Files | QDir::Readable | QDir::NoDotAndDotDot;
        if (isRecursive)
          filters |= (QDir::AllDirs | QDir::NoDotAndDotDot);
        fileInfoList = dir.entryInfoList(slFilters, filters);
        DebugLog(" - filtered file/dir count: %d", fileInfoList.count());
        for (int i = 0;i < fileInfoList.count();++i)
            if (fileInfoList.at(i).isFile())
                fileList.append(fileInfoList.at(i).absoluteFilePath());
            else if (isRecursive && fileInfoList.at(i).isDir() && fileInfoList.at(i).absoluteFilePath() != strDirectory)
            {
                DebugLog("iterating fn: %s abs: %s", qPrintable(fileInfoList.at(i).fileName()), qPrintable(fileInfoList.at(i).absoluteFilePath()));
                iterateDirectory(fileInfoList.at(i).absoluteFilePath(), isRecursive, slFilters);
            }
    }
}
//-------------------------------------------------------
int formMain::countFileLines(const QString& strFileName)
{
    int i, cnt = 0;
    QFile file(strFileName);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return 0;
    QList<QRegExp> regFilters;
    QTextStream in(&file);
    bool doCount;
    QString line;
    for (i = 0;i < ui->lstLineFilters->count();++i)
        regFilters.append(QRegExp(ui->lstLineFilters->item(i)->text()));
    while (!in.atEnd())
    {
        line = in.readLine();
        doCount = true;
        for (i = 0;doCount && i < regFilters.size();++i)
            if (line.contains(regFilters.at(i)))
            {
                doCount = false;
                break;
            }
        if (doCount)
            cnt++;
    }
    addResult(cnt, strFileName);
    return cnt;
}
//-------------------------------------------------------
void formMain::on_btnBrowse_clicked()
{
    QFileDialog diag(this);
    diag.setFileMode(QFileDialog::Directory);
    diag.setDirectory(ui->cbDirectory->currentText());
    if (diag.exec())
    {
        ui->cbDirectory->lineEdit()->setText(diag.selectedFiles().at(0));
    }
}
//-------------------------------------------------------
void formMain::on_btnAddFilter_clicked()
{
    bool ok;
    QString txtFileFilter = QInputDialog::getText(this, tr("New file wildcard")/*title*/,
                                           tr("New input file mask (Wildcard Expression):")/*text*/, QLineEdit::Normal,
                                           tr("*.cpp")/*default*/, &ok);
    if (ok && !txtFileFilter.isEmpty())
        ui->lstFilters->addItem(txtFileFilter);
}
//-------------------------------------------------------
void formMain::on_btnRemoveFilter_clicked()
{
    QListWidgetItem* item = ui->lstFilters->takeItem(ui->lstFilters->currentRow());
    if (item)
        delete item;
}
//-------------------------------------------------------
void formMain::on_btnAddComment_clicked()
{
    bool ok;
    QString txtFilter = QInputDialog::getText(this, tr("New line filter")/*title*/,
                                           tr("New line filter (Regular Expression):")/*text*/, QLineEdit::Normal,
                                           tr("^[ \t]*\r?\n?$")/*default*/, &ok);
    if (ok && !txtFilter.isEmpty())
        ui->lstLineFilters->addItem(txtFilter);
}
//-------------------------------------------------------
void formMain::on_btnRemoveComment_clicked()
{
    QListWidgetItem* item = ui->lstLineFilters->takeItem(ui->lstLineFilters->currentRow());
    if (item)
        delete item;
}
//-------------------------------------------------------
void formMain::addResult(int lineCount, const QString& strFileName)
{
    int cnt = ui->tableResult->rowCount();
    NumTableWidgetItem* itemCount = new NumTableWidgetItem(lineCount);
    QTableWidgetItem* itemFileName = new QTableWidgetItem(strFileName);
    itemCount->setTextAlignment(Qt::AlignRight);
    ui->tableResult->setRowCount(cnt+1);
    ui->tableResult->setItem(cnt, 0,  itemCount);
    ui->tableResult->setItem(cnt, 1,  itemFileName);
}
//-------------------------------------------------------
void formMain::save()
{
    int i, cnt;
    QSettings settings(tr("SoCo Software"), tr("Source Line Counter"));
    settings.setValue(tr("cfg-ver"), 1);

    settings.setValue(tr("root-dir"), ui->cbDirectory->currentText());
    cnt = ui->cbDirectory->count();
    if (cnt > 10) cnt = 10; // limmit root directory history to 10 entries
    settings.setValue(tr("root-dir-history-count"), cnt);
    for (i = 0;i < cnt;i++)
        settings.setValue(tr("root-dir-history-") + QString().number(i), ui->cbDirectory->itemText(i));

    settings.setValue(tr("recursive"), (ui->cbRecursive->isChecked() ? 1 : 0));

    settings.setValue(tr("profile-count"), 1);

    settings.setValue(tr("profile-1-name"), tr("Default"));

    settings.setValue(tr("profile-1-wild-count"), ui->lstFilters->count());
    for (i = 0;i < ui->lstLineFilters->count();i++)
        settings.setValue(tr("profile-1-wild-") + QString().number(i), ui->lstFilters->item(i)->text());

    settings.setValue(tr("profile-1-filter-count"), ui->lstLineFilters->count());
    for (i = 0;i < ui->lstLineFilters->count();i++)
        settings.setValue(tr("profile-1-filter-") + QString().number(i), ui->lstLineFilters->item(i)->text());
}
//-------------------------------------------------------
void formMain::load()
{
    int i, ii, ver, countRootDir, countProfile, countWild, countFilter;
    QSettings settings("SoCo Software", "Source Line Counter");
    ver = settings.value("cfg-ver").toInt();

    ui->cbDirectory->clear();
    //ui->cbDirectory->clearEditText();
    ui->cbDirectory->setEditText(settings.value(tr("root-dir")).toString());

    countRootDir = settings.value(tr("root-dir-history-count")).toInt();
    for (i = 0;i < countRootDir;i++)
        ui->cbDirectory->addItem(settings.value(tr("root-dir-history-") + QString().number(i)).toString());



    ui->cbRecursive->setChecked( !(settings.value(tr("recursive")) == 0) );
    countProfile = settings.value(tr("profile-count")).toInt();
    for (i = 0;i < countProfile;i++)
    {
        if (i == 0) // only one profile currently supported
        {
            ui->lstFilters->clear();
            countWild = settings.value(tr("profile-1-wild-count")).toInt();
            for (ii = 0;ii < countWild;ii++)
                ui->lstFilters->addItem(settings.value(tr("profile-1-wild-") + QString().number(ii)).toString());

            ui->lstLineFilters->clear();
            countFilter = settings.value(tr("profile-1-filter-count")).toInt();
            for (ii = 0;ii < countFilter;ii++)
                ui->lstLineFilters->addItem(settings.value(tr("profile-1-filter-") + QString().number(ii)).toString());

        }

    }

}
//-------------------------------------------------------
void formMain::on_lblWebLink_linkActivated(QString link)
{
    QDesktopServices::openUrl(QUrl("http://www.socosoftware.com"));
}
//-------------------------------------------------------
