# -------------------------------------------------
# Project created by QtCreator 2010-02-14T00:53:00
# -------------------------------------------------
QT += widgets
TARGET = SLC
TEMPLATE = app
SOURCES += main.cpp \
    formmain.cpp \
    debuglog.cpp \
    numtablewidgetitem.cpp
HEADERS += formmain.h \
    debuglog.h \
    numtablewidgetitem.h
FORMS += formmain.ui
OTHER_FILES += DebugLog.txt \
    ../SLC-build-desktop/DebugLog.txt \
    slc.rc

#RESOURCES += slc.qrc

RC_FILE += slc.rc
