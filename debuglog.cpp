//---------------------------------------------------------------------------
#include <iostream>
#include <fstream>
#include <stdarg.h>
#include "debuglog.h"
//---------------------------------------------------------------------------
char strDebugLogFileName[DebugLog_FileNameBufferSize]; // Extern
char DebugLogBuffer[DebugLog_FormatBufferSize];
//---------------------------------------------------------------------------
void DebugLog(const char* str, ...)
{
    //bool doArchive = false;
    std::ofstream f;
    va_list arglist;
    //---------------------------
    va_start(arglist, str);
    vsprintf(DebugLogBuffer, str, arglist);
    va_end(arglist);
    //---------------------------
    try
    {
        //---------------------------
        f.open(DebugLog_FileName, std::ios::app);
        f << DebugLogBuffer << std::endl;
        //---------------------------
        //if (DebugLog_FormatBufferSize && f.tellp() > DebugLog_ArchiveFileSize)
        //    doArchive = true;
        //---------------------------
    }
    catch (...)
    {
            ///
    }
    //---------------------------
    f.close();
    //---------------------------
    //if (doArchive)
    //    DebugLogArchive();
    //---------------------------
}
//---------------------------------------------------------------------------
